

all: libutepnum.a

integers/integer_ops.o: integers/integer_ops.c include/integer_ops.h
	gcc -Iinclude -Wall -O0 -g -c integers/integer_ops.c -o $@

widefloat/widefloat_ops.o: widefloat/widefloat_ops.c include/integer_ops.h include/widefloat_ops.h
	gcc -Iinclude -Wall -O0 -g -c widefloat/widefloat_ops.c -o $@

libutepnum.a: integers/integer_ops.o widefloat/widefloat_ops.o
	ar -rv $@ $^

test: tests/test_integers 

	tests/test_integers 1 134345 17 4223465
	tests/test_integers 2 3 95474567659999767676 54323115442
	tests/test_integers 123 1 1745 42
	tests/test_integers 23333 3345455555 232399999917 8888856568842
	
	
tests/test_integers: libutepnum.a tests/test_integers.o
	gcc -Iinclude -L. -Wall -O0 -g -o $@ tests/test_integers.o libutepnum.a

tests/test_integers.o: tests/test_integers.c include/utepnum.h
	gcc -Iinclude -Wall -O0 -g -c tests/test_integers.c -o $@


clean:
	rm -f libutepnum.a
	rm -f integers/integer_ops.o
	rm -f widefloat/widefloat_ops.o
	rm -f tests/test_integers.o
	rm -f tests/test_integers


.PHONY: all clean test
